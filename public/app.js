
window.addEventListener("load", function () {

    var button = document.getElementById("btn-submit"),
            searchField = document.getElementById("search-field"),
            requete = null, //La chaine de caractère à envoyer à l'api
            nbMaxReponse = 10,
            result = document.getElementById("result");

    //Evenement click sur le bouton
    button.addEventListener("click", function (e) {
        ajaxRequest(searchField.value, nbMaxReponse)
    }, false);

}, false)

/*
 * Fonction gérant la requête ajax
 */
function ajaxRequest(requete, nbMaxReponse) {
    req = new XMLHttpRequest();
    req.onreadystatechange = function (event) {
        // XMLHttpRequest.DONE === 4
        if (this.readyState === XMLHttpRequest.DONE) {
            if (this.status === 200) {
                hydrateDom(this.responseText);
            } else {
                return console.log(this.status + " - " + this.statusText);
            }
        }
    };
    req.open('GET', "http://localhost:1234/api?action=opensearch&format=json&formatversion=2&search=" + requete + "&namespace=0&limit=" + nbMaxReponse + "&suggest=true", true);
    req.send(null);
}

/*
 * Permet d'ajouter la reponse au dom
 */
function hydrateDom(texte) {
    result.innerHTML = "";//On vide le resultat

    var JsonObject = JSON.parse(texte),
            list = document.createElement("ul");
     
    if (JsonObject.error) {//Si le champs est vide
        switch (JsonObject.error.code) {
            case "nosearch":
                return result.innerHTML = "Veuillez remplir le champs";
        }
    } else if (JsonObject[1] == "") {//Si le service de retourne aucun résultat
        return result.innerHTML = "Aucun résultat trouvé"
    } else {
        for (var i = 0; i < JsonObject[1].length; i++) {
            var element = document.createElement("li");
            element.innerHTML = "<a href='" + JsonObject[3][i] + "' target='_blank'> " + JsonObject[1][i] + "</a>" + " " + JsonObject[2][i] + "<br><br>";
            list.appendChild(element);
        }
        return result.appendChild(list);
    }
    
}




